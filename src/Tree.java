import java.util.ArrayList;

public class Tree {
	private Node root;
	private ArrayList<Node> list;

	/**
	 * Constructor for class Tree
	 * @param entrance
	 * @param mazesize
	 * @param searcher
	 */
	public Tree(int[] entrance) {
		root = new Node(null, entrance, 0, "right");
		list = new ArrayList<Node>();
		list.add(root);
	}

	/**
	 * Method to add child to node
	 * @param coparent
	 * @param coordinates
	 * @param direction
	 */
	public void addChild(int[] coparent, int[] coordinates, String direction) {
		Node parent = getNode(coparent);
		if (parent == null)
			throw new IllegalArgumentException("Parent not found!");
		Node peter = new Node(parent, coordinates, parent.getHeight() + 1, direction);
		parent.addChild(peter);
		list.add(peter);
	}

	/**
	 * Search the coordinates of the first node a given height
	 * @param height
	 * @return
	 */
	public int[] searchFirstOfHeight(int height) {
		for (Node node : list) {
			if (height == node.getHeight()) {
				return node.getCoordinates();
			}
		}
		return new int[]{0,0};
	}
	
	
	/**
	 * Search the coordinates of the next node in the list
	 * @param coord
	 * @return
	 */
	public int[] searchNext(int[] coord) {
		return list.get(getIndex(coord)+1).getCoordinates();
	}
	
	/**
	 * Calculates how many nodes of a given height are in the list
	 * @param height
	 * @return
	 */
	public int amountOfHeight(int height) {
		int count = 0;
		for (Node node : list) 
		{
			if (height == node.getHeight()) 
			{
				count++;
			}
		}
		
		return count;
	}

	/**
	 * Method to get the coordinates from the parent of a node, of which the coordinates are given
	 * @param coord
	 * @return
	 */
	public int[] getParent(int[] coord) {
		for (Node node : list) {
			if (coord[0] == node.getCoordinates()[0] && coord[1] == node.getCoordinates()[1]) {
				if (node.getParent() != null)
					return node.getParent().getCoordinates();
				else
					return null;
			}
		}
		return null;
	}
	
	/**
	 * Getter for the size of the list of all the nodes
	 * @return
	 */
	public int size() {
		return list.size();
	}

	/**
	 * Getter for direction of given coordinates
	 * @param coord
	 * @return
	 */
	public String getDirection(int[] coord) {
		return getNode(coord).getDirection();
	}
	
	/**
	 * Getter for the index of node of given coordinates in the list of all nodes
	 * @param coord
	 * @return
	 */
	private int getIndex(int[] coord) {
		for (Node node : list) {
			if (coord[0] == node.getCoordinates()[0] && coord[1] == node.getCoordinates()[1]) {
				return list.indexOf(node);
			}
		}
		return 0;
	}

	/**
	 * Private method to get node of given coordinates
	 * @param coordinates
	 * @return
	 */
	private Node getNode(int[] coordinates) {
		for (Node node : list) {
			if (coordinates[0] == node.getCoordinates()[0] && coordinates[1] == node.getCoordinates()[1]) {
				return node;
			}
		}
		
		return null;
	}

	private class Node {
		private int[] coordinates;
		private int height;
		private String direction;
		private Node parent;
		private ArrayList<Node> children;

		/**
		 * Constructor for class Node
		 * @param parent
		 * @param coordinates
		 * @param height
		 * @param direction
		 */
		public Node(Node parent, int[] coordinates, int height, String direction) {
			this.direction = direction;
			this.height = height;
			this.coordinates = coordinates;
			this.parent = parent;
			children = new ArrayList<Node>();
		}

		/*
		 * Getter for the height of the node in the tree
		 */
		public int getHeight() {
			return height;
		}

		/**
		 * Method to add child to node
		 * @param peter
		 */
		public void addChild(Node peter) {
			children.add(peter);
		}

		/**
		 * Getter for the coordinates
		 * @return
		 */
		public int[] getCoordinates() {
			return coordinates;
		}

		/**
		 * Getter for the parent node
		 * @return
		 */
		public Node getParent() {
			return parent;
		}

		/**
		 * Getter for the direction
		 * @return
		 */
		public String getDirection() {
			return direction;
		}
	}
}
