import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.*;
import java.io.IOException;
import java.util.stream.*;

public class Searcher {
	private static final String FILENAME = "..\\GameSolver\\src\\maze.txt";
	private static final String right = "right", left = "left", up = "up", down = "down";
	private Tree tree;

	// 3 matrices: one with walls, one with junctions and one with the path to the
	// exit
	private int[][] maze, junctions, solution;

	// Coordinates of the entrance and exit
	private int[] entrance, exit;

	// size of maze
	private int mazesize;

	// boolean variables
	private boolean begin = true, solving, searching = true;

	/**
	 * Constructor for class Searcher
	 * @throws Exception 
	 */
	public Searcher() throws Exception {
		// read maze-file: construct maze[][] and set mazesize
		readfile();

		// initialize matrices
		junctions = new int[mazesize][mazesize];
		solution = new int[mazesize][mazesize];

		// initialize coordinates entrance and exit
		findEntrance();
		findExit();

		// Find all junctions in maze and put them in matrix junctions[][]
		findAllJunctions();

		// Times duration of search algorithm
		long startTime = System.nanoTime();

		// search for exit from entrance
		createTree();

		// Reproduce path from exit to entrance
		solution();

		// timer
		long endTime = System.nanoTime();
		long duration = (endTime - startTime) / 1000000;
		System.out.println(duration + " ms");

		// Render maze for visual control
		new RenderMaze(this);
	}

	/**
	 * Method to construct maze from textfile in matrix maze[][] and sets the
	 * mazesize
	 * @throws Exception 
	 */
	public void readfile () throws Exception
	{		
		try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
			String sCurrentLine;
			int y = 0;
			while ((sCurrentLine = br.readLine()) != null) {
				if (begin) 
				{
					mazesize = sCurrentLine.length();
					maze = new int[mazesize][mazesize];
					begin = false;
				}
				
				for (int x = 0; x < mazesize; x++) 
				{
					maze[x][y] = sCurrentLine.charAt(x);
				}
				
				y++;
			}
			
			if (mazesize != y) 
			{
				throw new Exception("Maze should be a square!");
			}
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * Getter for size of the maze
	 * 
	 * @return mazesize
	 */
	public int getMazeSize() {
		return mazesize;
	}

	/**
	 * Getter for the maze itself
	 * 
	 * @return matrix maze
	 */
	public int[][] getMaze() {
		return maze;
	}

	/**
	 * Find and set coordinates entrance
	 */
	public void findEntrance() {
		for (int i = 0; i < mazesize; i++) {
			if (maze[0][i] == 32) {
				entrance = new int[] { 0, i };
				break;
			}
		}
	}

	/**
	 * Find and set coordinates exit
	 */
	public void findExit() {
		for (int i = 0; i < mazesize; i++) {
			if (maze[mazesize - 1][i] == 32) {
				exit = new int[] { mazesize - 1, i };
				break;
			}
		}
	}

	/**
	 * Getter for entrance
	 * 
	 * @return coordinates entrance
	 */
	public int[] getEntrance() {
		return entrance;
	}

	/**
	 * getter for exit
	 * 
	 * @return coordinates exit
	 */
	public int[] getExit() {
		return exit;
	}

	/**
	 * Method to find all the junctions in the maze Put coordinates in a matrix
	 */
	public void findAllJunctions() {
		for (int k = 1; k < mazesize - 1; k++) {
			for (int l = 1; l < mazesize - 1; l++) {
				if (maze[k][l] == 32) {
					int counter = 0;
					if (maze[k + 1][l] == 32)
						counter++;
					if (maze[k - 1][l] == 32)
						counter++;
					if (maze[k][l + 1] == 32)
						counter++;
					if (maze[k][l - 1] == 32)
						counter++;
					if (counter > 2)
						junctions[k][l] = counter;
					else
						junctions[k][l] = 0;
				}
			}
		}
	}

	/**
	 * follows path to next junction
	 * 
	 * @param x
	 * @param y
	 * @param dir
	 * @param parent
	 */
	public void move(int x, int y, String flag, int[] parent) {
		while (true) 
		{
			if (maze[x + 1][y] == 32 && flag != left) 
			{
				x++;
				flag = right;
			} 
			else if (maze[x][y - 1] == 32 && flag != down) 
			{
				y--;
				flag = up;
			} 
			else if (maze[x][y + 1] == 32 && flag != up) 
			{
				y++;
				flag = down;
			} 
			else if (maze[x - 1][y] == 32 && flag != right) 
			{
				x--;
				flag = left;
			} 
			else 
			{
				break;
			}

			// will execute if in method solution()
			if (solving) {
				solution[x][y] = 5;
				if (junctions[x][y] > 2) {
					break;
				}
				if (x == 0) {
					solving = false;
					break;
				}
			}

			// will execute if in method createTree()
			else {
				if (junctions[x][y] > 2) {
					tree.addChild(parent, new int[] { x, y }, flag);
					break;
				}
				if (x == exit[0]) {
					tree.addChild(parent, new int[] { x, y }, flag);
					searching = false;
					break;
				}
			}
		}
	}

	/**
	 * Method to find the next junction in the maze following a given direction
	 * 
	 * @param x
	 * @param y
	 * @param dir
	 */
	public void findNextJunction(int x, int y, String dir) {
		// initialize parent
		int[] parent = new int[] { x, y };

		// the coordinates(x, y) are those of a junction
		// Therefore we move immediately in given direction to force the mover in this
		// direction
		switch (dir) {
		case left:
			x--;
			break;
		case right:
			x++;
			break;
		case up:
			y--;
			break;
		case down:
			y++;
			break;
		}

		// Add coordinates to solution
		if (solving) {
			solution[x][y] = 5;
		}

		// Check if position is the exit
		if (x == exit[0] && y == exit[1]) {
			tree.addChild(parent, new int[] { x, y }, right);
			searching = false;
			return;
		}

		// Check if position is a wall
		if (maze[x][y] != 32) {
		}

		// Follows path to next junction
		else {
			move(x, y, dir, parent);
		}
	}

	/**
	 * Method to recreate the path from exit to entrance
	 */
	public void solution() {
		solving = true;
		int[] temp = exit;

		// Check if path left from exit is already a junction
		// If so search will start from there
		if (tree.getParent(temp)[0] == exit[0] - 1 && tree.getParent(temp)[1] == exit[1]) {
			temp = tree.getParent(temp);
			solution[temp[0]][temp[1]] = 5;
		}

		//
		while (solving) {
			String dir = "";
			switch (tree.getDirection(temp)) {
			case left:
				dir = right;
				break;
			case right:
				dir = left;
				break;
			case up:
				dir = down;
				break;
			case down:
				dir = up;
				break;
			}
			solution[temp[0]][temp[1]] = 5;
			findNextJunction(temp[0], temp[1], dir);
			temp = tree.getParent(temp);
			if (temp == entrance)
				break;
		}
		solving = false;
	}

	/**
	 * Getter for the solution
	 * 
	 * @return
	 */
	public int[][] getSolution() {
		return solution;
	}

	/**
	 * Method to find exit beginning from entrance
	 */
	public void createTree() {
		tree = new Tree(entrance);
		for (int i = 0; searching; i++) {
			int[] coord = tree.searchFirstOfHeight(i);
			int amount = tree.amountOfHeight(i);
			
			for (int j = 0; j < amount; j++) 
			{
				String direction = tree.getDirection(coord);
				
				if (direction != right)
					findNextJunction(coord[0], coord[1], left);
				if (direction != left)
					findNextJunction(coord[0], coord[1], right);
				if (direction != down)
					findNextJunction(coord[0], coord[1], up);
				if (direction != up)
					findNextJunction(coord[0], coord[1], down);
				
				coord = tree.searchNext(coord);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		new Searcher();
	}
}
