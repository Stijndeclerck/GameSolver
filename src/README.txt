Ik heb gekozen om te werken met een tree om het doolhof op te lossen.
Aangezien het een perfect maze is, is er maar 1 weg naar de uitgang en zitten er geen loops in het doolhof.

Stappen:
1 Het document wordt uitgelezen en het doolhof opgeslagen in een matrix
2 Aan de hand van die matrix wordt de ingang, de uitgang en alle splitsingen gezocht.
3 Een tree wordt gecre�erd. De root is de ingang met als element zijn coordinaten en als height 0.
4 Het pad wordt gezocht en gevolgd tot de eerste splitsing.
5 Splitsing: 		- opgeslagen als node
			- wordt als child van de node waaruit vertrokken werd (eerste keer ingang) toegevoegd.
			- krijgt zelf als parent die node.
			- coordinaten worden opgeslagen.
			- krijgt height parent+1.
			- laatst gevolgde richting wordt opgeslagen.
			- wordt toegevoegd aan een ArrayList die alle nodes bevat.

6 Nu wordt de eerst opgeslagen node met height 1 opgezocht in die ArrayList.
  Alsook het aantal nodes met die height.
  Zo kunnen we alle nodes met die height overlopen.

7 Bij alle nodes wordt gecontroleerd of er naar links, rechts, boven of onder kan gegaan worden, 
  behalve in de richting waaruit de node initieel gevonden werd, om zo niet terug te keren in het doolhof.
  Indien mogelijk wordt in die richting verder gezocht naar de volgende splitsing.

8 Indien een splitsing wordt gevonden wordt stap 5 herhaald.
  Indien niet gebeurt er niets.

9 Nu worden alle nodes met height 2 gecontroleerd en zo verder...

10 Het algoritme zal zo geleidelijk aan doorheen heel het doolhof lopen totdat het de uitgang gevonden heeft.

11 De weg wordt nu gereconstrueerd vertrekkende vanuit de uitgang.

12 Aangezien iedere keer een richting werd opgeslagen bij iedere node, moet bij het vertrek vanuit die node gewoon 
  in de tegengestelde richting gegaan worden tot het zijn parent tegenkomt.

13 Ieder vakje die nu overlopen wordt wordt opgeslagen in een nieuwe matrix.

14 De uitweg is gevonden!


Achteraf gebleken lijkt mij dit zeker niet de meest effici�nte methode om de uitweg te zoeken.
Die weg blijkt meestal diegene die de meeste splitsingen tegenkomt, waardoor uiteindelijk het gehele doolhof wordt gescreend.
