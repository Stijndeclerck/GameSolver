import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JFrame;

public class RenderMaze extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final int blockwidth = 15, blockheight = 15;
	private Searcher searcher;
	private int mazesize;

	/**
	 * Constructor for class RenderMaze
	 * 
	 * @param searcher
	 */
	public RenderMaze(Searcher searcher) {
		this.searcher = searcher;
		mazesize = searcher.getMazeSize();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		pack();
		setSize(new Dimension(blockwidth * (mazesize + 2), blockheight * (mazesize + 4)));
		setLocationRelativeTo(null);
		setVisible(true);
		repaint();
	}

	/**
	 * Paint maze elements on JFrame
	 */
	public void paint(Graphics g) {
		super.paintComponents(g);
		int a = 1;
		for (int k = 0; k < mazesize; k++) {
			for (int l = 0; l < mazesize; l++) {
				// Render walls of maze
				if (searcher.getMaze()[l][k] == 88) {
					g.setColor(Color.BLUE);
					g.fillRect((l + 1) * blockwidth, (k + 3) * blockheight, blockwidth - a, blockheight - a);
					g.setColor(Color.BLACK);
					g.drawRect((l + 1) * blockwidth, (k + 3) * blockheight, blockwidth, blockheight);
				}

				// Render the solution
				if (searcher.getSolution()[l][k] == 5) {
					g.setColor(Color.YELLOW);
					g.fillRect((l + 1) * blockwidth, (k + 3) * blockheight, blockwidth - a, blockheight - a);
					g.setColor(Color.BLACK);
					g.drawRect((l + 1) * blockwidth, (k + 3) * blockheight, blockwidth, blockheight);
				}
			}
		}

		// Render Entrance and Exit
		g.setColor(Color.GREEN);
		g.fillRect(blockwidth, blockheight * (searcher.getEntrance()[1] + 3), blockwidth - a, blockheight - a);
		g.fillRect(blockwidth * mazesize, blockheight * (searcher.getExit()[1] + 3), blockwidth - a, blockheight - a);
		g.setColor(Color.BLACK);
		g.drawRect(blockwidth, blockheight * (searcher.getEntrance()[1] + 3), blockwidth, blockheight);
		g.drawRect(blockwidth * mazesize, blockheight * (searcher.getExit()[1] + 3), blockwidth, blockheight);
	}
}
